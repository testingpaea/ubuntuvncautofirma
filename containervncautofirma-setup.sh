#!/bin/bash
echo user passed $withUser - password $withPassword
export ENV_USER=${withUser:-sae}
export ENV_PASSWORD=${withPassword:-$ENV_USER}
export userPath="/home/$ENV_USER"
echo $userPath -- $username

/usr/bin/containervncsmartcard-setup.sh

echo "using certutil to register autofirma cert"
su -c 'certutil -d sql:$userPath/.pki/nssdb -A -n "AutoFirma" -i /usr/share/ca-certificates/AutoFirma/AutoFirma_ROOT.crt  -t TCP,TCP,TCP' $ENV_USER
su -c 'certutil -A -d sql:$userPath/.pki/nssdb -n cmfmroot  -t TCP,TCP,TCP -i /tmp/certs/root_chambers-2008.cer' $ENV_USER
su -c 'certutil -A -d sql:$userPath/.pki/nssdb -n cmfmrootInterm  -t TCP,TCP,TCP -i /tmp/certs/camerfirma_cserverii-2015.cer' $ENV_USER

echo "setup of firefox handlers"
cp /tmp/firefox/handlers.json "$userPath/.mozilla/firefox/$(ls $userPath/.mozilla/firefox/ | grep default)/handlers.json"
chown $ENV_USER:$ENV_USER $userPath/.mozilla/firefox/$(ls $userPath/.mozilla/firefox/ | grep default)/handlers.json
